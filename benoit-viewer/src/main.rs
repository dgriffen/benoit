extern crate benoit;
extern crate image as im;
extern crate piston_window;

use benoit::{Bounds, Mandelbrot, Render, Screen};
use im::ImageBuffer;
use piston_window::*;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;

fn main() {
    let mut window: PistonWindow = WindowSettings::new("benoit viewer", [300, 300])
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut bounds = Bounds {
        min_x: -2.0,
        max_x: 2.0,
        min_y: -2.0,
        max_y: 2.0,
    };

    let mut receiver = create_render(200, bounds);
    let mut render = get_coord_iter(receiver.recv().unwrap());
    let mut m_x = 0.0;
    let mut m_y = 0.0;
    while let Some(e) = window.next() {
        if let Ok(new_render) = receiver.try_recv() {
            println!("{:?}:{:?}", new_render.rows, new_render.columns);

            render = get_coord_iter(new_render);
        }

        if let Some(mouse) = e.mouse_cursor_args() {
            m_x = mouse[0];
            m_y = mouse[1];
            println!("x{}y{}", m_x, m_y);
        }

        if let Some(scroll) = e.mouse_scroll_args() {
            let magnitude = (bounds.max_x - bounds.min_x);
            let x = (m_x / 300.0) * magnitude + bounds.min_x;
            let y = bounds.max_y - (m_y / 300.0) * magnitude;

            println!("x{}y{}", x, y);
            let zoom = scroll[1];
            let magnitude = if zoom > 0.0 {
                (bounds.max_x - bounds.min_x) / 4.0
            } else {
                (bounds.max_x - bounds.min_x) * 4.0
            };

            bounds.min_x = x - magnitude;
            bounds.max_x = x + magnitude;
            bounds.min_y = y - magnitude;
            bounds.max_y = y + magnitude;

            receiver = create_render((50.0 / magnitude) as u64, bounds);
        }

        let sx = 300.0 / render.width() as f64;
        let sy = 300.0 / render.height() as f64;
        let texture =
            Texture::from_image(&mut window.factory, &render, &TextureSettings::new()).unwrap();
        window.draw_2d(&e, |c, g| {
            clear([0.5; 4], g);
            image(&texture, c.transform.scale(sx, sy), g);
        });
    }
}

fn get_coord_iter(render: Render) -> ImageBuffer<im::Rgba<u8>, Vec<u8>> {
    let columns = render.columns;
    let rows = render.rows;
    let render = render
        .escapes
        .into_iter()
        .enumerate()
        .map(move |(i, escape)| {
            let row = i / columns;
            let column = i % columns;
            let color = match escape {
                Some(mut speed) => {
                    let mut red = 255;
                    let mut gb = 255;
                    while speed > 0 {
                        if red > 10 {
                            red /= 12;
                            red *= 10;
                        } else {
                            gb /= 12;
                            gb *= 10;
                        }
                        speed -= 1;
                    }
                    [red, gb, 255, 255]
                }
                None => [0, 0, 0, 255],
            };
            (row, column, color)
        });

    let mut image = ImageBuffer::new(columns as u32, rows as u32);
    for (row, column, color) in render {
        image.put_pixel(column as u32, row as u32, im::Rgba(color));
    }

    image
}

fn create_render(iterations: u64, bounds: Bounds) -> Receiver<Render> {
    let (sender, receiver) = channel();
    thread::spawn(move || {
        let screen = Screen {
            rows: 300,
            columns: 300,
            bounds,
        };
        let mandlebrot = Mandelbrot::new(screen, iterations);
        for render in mandlebrot {
            sender.send(render).unwrap();
        }
    });

    receiver
}
