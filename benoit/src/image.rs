#[derive(Clone, Copy)]
pub struct Pixel {
    pub col: usize,
    pub row: usize,
}

#[derive(Clone, Copy)]
pub struct Bounds {
    pub min_x: f64,
    pub max_x: f64,
    pub min_y: f64,
    pub max_y: f64,
}

#[derive(Clone, Copy)]
pub struct Screen {
    pub columns: usize,
    pub rows: usize,
    pub bounds: Bounds,
}
