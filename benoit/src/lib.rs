extern crate num_complex;
extern crate rayon;

mod image;

pub use image::{Bounds, Pixel, Screen};
use num_complex::Complex;
use rayon::prelude::*;

pub struct Mandelbrot {
    screen: Screen,
    rendered: Vec<Option<Option<u64>>>,
    zoom: u32,
    iterations: u64,
}

impl Mandelbrot {
    pub fn new(screen: Screen, iterations: u64) -> Self {
        let zoom = max_zoom(screen.columns, screen.rows) + 1;
        Mandelbrot {
            screen,
            zoom,
            rendered: vec![None; screen.columns * screen.rows],
            iterations,
        }
    }
}

#[derive(Debug)]
pub struct Render {
    pub escapes: Vec<Option<u64>>,
    pub columns: usize,
    pub rows: usize,
}

impl Iterator for Mandelbrot {
    type Item = Render;

    fn next(&mut self) -> Option<Self::Item> {
        if self.zoom == 0 {
            return None;
        }

        self.zoom -= 1;
        let zoom = self.zoom;
        let screen = self.screen;
        let iterations = self.iterations;
        self.rendered
            .par_chunks_mut(self.screen.columns)
            .enumerate()
            .filter(|(i, _)| row_in_zoom(zoom, *i))
            .for_each(|(i, row)| compute_row(screen, zoom, iterations, i, row));

        let columns = (self.screen.columns + zoom_col_size(zoom) - 1) / zoom_col_size(zoom);
        let rows = (self.screen.rows + zoom_row_size(zoom) - 1) / zoom_row_size(zoom);

        let escapes = self.rendered
            .chunks(self.screen.columns)
            .enumerate()
            .filter(|(row_n, _)| row_in_zoom(zoom, *row_n))
            .flat_map(|(_, row)| {
                row.iter()
                    .enumerate()
                    .filter(|(col_n, _)| col_in_zoom(zoom, *col_n))
            })
            .map(|(_, escape)| escape.unwrap())
            .collect();

        Some(Render {
            escapes,
            columns,
            rows,
        })
    }
}

fn compute_row(
    screen: Screen,
    zoom: u32,
    iterations: u64,
    row_n: usize,
    row: &mut [Option<Option<u64>>],
) {
    row.iter_mut()
        .enumerate()
        .filter(|(_, col)| col.is_none())
        .filter(|(col_n, _)| col_in_zoom(zoom, *col_n))
        .for_each(|(col_n, col)| {
            let (x, y) = pixel_to_coord(
                screen,
                Pixel {
                    row: row_n,
                    col: col_n,
                },
            );
            let location = Complex::new(x, y);
            *col = Some(test_location(location, iterations));
        });
}

fn pixel_to_coord(screen: Screen, pixel: Pixel) -> (f64, f64) {
    let width = screen.bounds.max_x - screen.bounds.min_x;
    let height = screen.bounds.max_y - screen.bounds.min_y;

    let x = screen.bounds.min_x + (((pixel.col as f64 + 0.5) / screen.columns as f64) * width);
    let y = screen.bounds.max_y - (((pixel.row as f64 + 0.5) / screen.rows as f64) * height);

    (x, y)
}

fn test_location(location: Complex<f64>, max_iterations: u64) -> Option<u64> {
    let mut z = Complex::new(0.0, 0.0);
    for i in 0..max_iterations {
        z = (z * z) + location;
        if z.norm_sqr() > 4.0 {
            return Some(i);
        }
    }

    None
}

fn max_zoom(columns: usize, rows: usize) -> u32 {
    let mut zoom = 0;
    while zoom_row_size(zoom) < rows || zoom_col_size(zoom) < columns {
        zoom += 1;
    }

    zoom
}

fn zoom_row_size(zoom: u32) -> usize {
    1 << ((zoom + 1) / 2)
}

fn zoom_col_size(zoom: u32) -> usize {
    1 << (zoom / 2)
}

fn row_in_zoom(zoom_level: u32, row: usize) -> bool {
    row % zoom_row_size(zoom_level) == 0
}

fn col_in_zoom(zoom_level: u32, col: usize) -> bool {
    col % zoom_col_size(zoom_level) == 0
}

#[cfg(test)]
mod test {
    use super::image::{Bounds, Pixel, Screen};
    use num_complex::Complex;

    #[test]
    fn test_in_set() {
        let location = Complex::new(0.0, 0.0);
        assert!(super::test_location(location, 5).is_none());
    }

    #[test]
    fn test_outside_set() {
        let location = Complex::new(1.1, 0.0);
        assert!(super::test_location(location, 5).is_some());
    }

    #[test]
    fn test_edge_small_iter() {
        let location = Complex::new(-0.75, 0.025);
        assert!(super::test_location(location, 5).is_none());
    }

    #[test]
    fn test_edge_large_iter() {
        let location = Complex::new(-0.75, 0.025);
        assert!(super::test_location(location, 150).is_some());
    }

    #[test]
    fn pixel_to_coord_zero_center() {
        let bounds = Bounds {
            min_x: -2.0,
            max_x: 2.0,
            min_y: -2.0,
            max_y: 2.0,
        };
        let screen = Screen {
            columns: 15,
            rows: 15,
            bounds,
        };

        let coord = super::pixel_to_coord(screen, Pixel { row: 7, col: 7 });
        assert_eq!(coord, (0.0, 0.0));
    }

    #[test]
    fn pixel_to_coord_irregular_center() {
        let bounds = Bounds {
            min_x: -1.5,
            max_x: 0.5,
            min_y: -0.5,
            max_y: 1.5,
        };
        let screen = Screen {
            columns: 15,
            rows: 15,
            bounds,
        };

        let coord = super::pixel_to_coord(screen, Pixel { row: 7, col: 7 });
        assert_eq!(coord, (-0.5, 0.5));
    }

    #[test]
    fn all_rows_in_zoom_zero() {
        for i in 0..25 {
            assert!(super::row_in_zoom(0, i));
        }
    }

    #[test]
    fn all_cols_in_zoom_zero() {
        for i in 0..25 {
            assert!(super::col_in_zoom(0, i));
        }
    }

    #[test]
    fn cols_in_large_zoom() {
        assert!(super::col_in_zoom(8, 0));
        assert!(super::col_in_zoom(8, 16));
        assert!(super::col_in_zoom(7, 8));
        assert!(super::col_in_zoom(7, 8));
    }

    #[test]
    fn rows_in_large_zoom() {
        assert!(super::row_in_zoom(8, 0));
        assert!(super::row_in_zoom(8, 16));
        assert!(!super::row_in_zoom(7, 8));
        assert!(!super::row_in_zoom(7, 8));
    }

    #[test]
    fn max_zoom() {
        assert_eq!(8, super::max_zoom(16, 16));
        assert_eq!(9, super::max_zoom(16, 17));
        assert_eq!(9, super::max_zoom(15, 18));
        assert_eq!(11, super::max_zoom(16, 33));
    }
}
