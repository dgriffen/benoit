extern crate benoit;
extern crate gif;

use benoit::{Bounds, Mandelbrot, Render, Screen};
use gif::Parameter;
use std::fs::File;
use std::io::BufWriter;

fn main() {
    let screen = Screen {
        columns: 2048,
        rows: 1024,
        bounds: Bounds {
            min_x: -2.4,
            max_x: 2.4,
            min_y: -1.2,
            max_y: 1.2,
        },
    };

    let brot = Mandelbrot::new(screen, 200);

    let file = File::create("brot.gif").unwrap();
    let mut image = BufWriter::new(file);
    let mut encoder =
        gif::Encoder::new(&mut image, screen.columns as u16, screen.rows as u16, &[]).unwrap();
    gif::Repeat::Infinite.set_param(&mut encoder).unwrap();
    for render in brot {
        let mut pixels = get_pixels(&render, screen.columns, screen.rows);
        println!(
            "columns: {}, rows: {}, raw:{}, expanded: {}",
            render.columns,
            render.rows,
            render.escapes.len(),
            pixels.len()
        );
        let new_cols = (screen.columns / render.columns) * render.columns;
        let new_rows = (screen.rows / render.rows) * render.rows;
        let mut frame = gif::Frame::from_rgb(new_cols as u16, new_rows as u16, &pixels);
        frame.delay = 50;
        encoder.write_frame(&frame).unwrap();
    }
}

fn get_pixels(render: &Render, full_cols: usize, full_rows: usize) -> Vec<u8> {
    render
        .escapes
        .chunks(render.columns)
        .flat_map(|row| {
            let repeats = full_rows / render.rows;
            (0..repeats).map(move |_| row.clone())
        })
        .flat_map(|row| row.into_iter())
        .flat_map(|col| {
            let repeats = full_cols / render.columns;
            (0..repeats).map(move |_| col)
        })
        .flat_map(|escape| match escape {
            Some(mut speed) => {
                let mut red = 255;
                let mut gb = 255;
                while speed > 0 {
                    if red > 10 {
                        red /= 12;
                        red *= 10;
                    } else {
                        gb /= 12;
                        gb *= 10;
                    }
                    speed -= 1;
                }
                vec![red, gb, 255]
            }
            None => vec![0, 0, 0],
        })
        .collect::<Vec<_>>()
}
